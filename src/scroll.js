import { log } from './log'


export let sectionNumber = 0;
const sections = document.getElementsByClassName('section')
export function handleScroll() {
    const scrollYPosition = window.scrollY;
    let lastSection = sectionNumber;

    for (let index = 0; index < sections.length; index++) {
        const section = sections[index];

        const sectionTop = section.offsetTop;
        const sectionHeight = section.offsetHeight;
        const sectionBottom = sectionTop + sectionHeight;

        if (scrollYPosition + window.innerHeight <= sectionBottom + 20) {
            sectionNumber = index + 1;
            break
        }
    }


    if (lastSection !== sectionNumber) {
        // log("Section: " + sectionNumber)
        const sectionChangeEvent = new CustomEvent('sectionchange', {
            bubbles: true,
            detail: { section: sectionNumber }
        });
        window.dispatchEvent(sectionChangeEvent);
    }
}