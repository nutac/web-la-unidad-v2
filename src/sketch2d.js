import p5 from 'p5';
let sketch = (p) => {
    let canvas;
    let points = [];

    p.setup = () => {
        canvas = p.createCanvas(window.innerWidth, window.innerHeight);
        canvas.parent('p5-container');
        p.background(255, 0); // Fondo transparente

        // Inicializa los puntos móviles
        for (let i = 0; i < 5; i++) {
            points.push(new MovingPoint(p.random(p.width), p.random(p.height)));
        }
    };

    p.draw = () => {
        //   p.background( 0); // Limpia el fondo en cada fotograma
        p.fill("#1e1e1e")
        p.rect(0, 0, p.width, p.height)

        // Dibuja la grilla
        drawGrid(p);

        // Actualiza y dibuja los puntos móviles
        points.forEach(point => {
            point.update();
            point.display();
        });
    };
    p.windowResized = () => {
        p.resizeCanvas(window.innerWidth, window.innerHeight);
        p.background(255, 0); 
    };

    function drawGrid(p) {
        let spacing = 20;
        for (let x = 0; x < p.width; x += spacing) {
            for (let y = 0; y < p.height; y += spacing) {
                p.stroke(0);
                p.strokeWeight(1);
                p.noFill();
                p.rect(x, y, spacing, spacing);
            }
        }
    }

    // Clase para representar los puntos móviles
    class MovingPoint {
        constructor(x, y) {
            this.x = x;
            this.y = y;
            this.speed = 0.25;
            this.directionX = p.random() < 0.5 ? 1 : -1; // Dirección horizontal
            this.directionY = p.random() < 0.5 ? 1 : -1; // Dirección vertical
            this.color = p.color('#b2ee2e');
            this.size = 15;
        }

        // Actualiza la posición del punto móvil
        update() {
            // Mover horizontalmente
            this.x += this.speed * this.directionX;

            // Revierte la dirección horizontal cuando llega al borde de la grilla
            if (this.x <= 0 || this.x >= p.width) {
                this.directionX *= -1;
            }

            // Mover verticalmente
            this.y += this.speed * this.directionY;

            // Revierte la dirección vertical cuando llega al borde de la grilla
            if (this.y <= 0 || this.y >= p.height) {
                this.directionY *= -1;
            }
        }

        // Dibuja el punto móvil en su posición actual
        display() {
            // Encuentra la celda de la grilla más cercana
            let cellSize = 20;
            let gridX = Math.floor(this.x / cellSize) * cellSize + cellSize / 2;
            let gridY = Math.floor(this.y / cellSize) * cellSize + cellSize / 2;

            // Dibuja el punto en la celda de la grilla más cercana
            p.fill(this.color);
            p.noStroke();
            // p.ellipse(gridX, gridY, this.size, this.size);
            p.rect(gridX - this.size * 0.5, gridY - this.size * 0.5, this.size, this.size);

        }
    }
};

new p5(sketch, 'p5-container');
