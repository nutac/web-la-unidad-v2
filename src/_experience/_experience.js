
import * as THREE from 'three';
import * as dom from "../dom-functions"
import { _ } from "../dom-functions"
import { setupParamsGui, debugMode, gui } from './params.js';
import { scrollToSection } from '../navBar.js';

import * as LIGHTS from './lights'
// import { debugMode } from '../index';
import * as MODELS from "./models.js"
import * as CAM from './camera'
import * as LABELS from './labels2d.js';
import { incrementLoaderPct } from './loader.js';
import { setupSections } from '../index.js';


// var SPECTOR = require("spectorjs");

// var spector = new SPECTOR.Spector();
// spector.displayUI();

var stats

let scene, renderer;
init();
async function init() {
    if (debugMode) {
        stats = new Stats();
        stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
        document.body.appendChild(stats.dom);
    }


    console.time("LOADING ALL")
    setupSections()

    scene = new THREE.Scene();
    // scene.background = new THREE.Color(params.backgroundColor);

    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setClearColor(0x000000, 0);
    renderer.domElement.style.background = 'none';
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.outputColorSpace = THREE.SRGBColorSpace;
    scene.outputColorSpace = THREE.SRGBColorSpace
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.gammaFactor = 2.2;
    renderer.toneMapping = THREE.ACESFilmicToneMapping;

    renderer.shadowMap.enabled = true;
    // renderer.shadowMap.type = THREE.PCFSoftShadowMap

    document.getElementById('canvasContainer').appendChild(renderer.domElement);

    LABELS.setupRenderer2D()
    CAM.setup(renderer, scene)
    LIGHTS.setup(scene)
    incrementLoaderPct();

    await MODELS.loadModels(scene, renderer)
    // // CAM.createPoints(MODELS.container)
    // CAM.createPoints(scene)

    // LIGHTS.addToContainer(MODELS.container)

    /// GUI
    await setupParamsGui()
    renderer.toneMappingExposure = 1.6
    gui.add(renderer, 'toneMappingExposure', 0, 2).name('exposure');

    incrementLoaderPct();

    window.addEventListener('resize', () => CAM.onWindowResize(renderer, LABELS.divRenderer), false);
    CAM.onWindowResize(renderer, LABELS.divRenderer)
    incrementLoaderPct();

    animate()
    // console.log("6 - animate set")
    incrementLoaderPct();

    if (dom.isMobile()) {
        MODELS.moveContainerDown(true, 0)
    } else {
        MODELS.moveContainerToRight(true, 0)
        MODELS.rotateContainerToRight(true, 0)
    }

    setTimeout(function () {
        dom.removeClass("spinner", "show")
        dom.addClass("btn-loader", "show")

        if (debugMode) {
            closeWelcome()
        } else {
            scrollToSection(0)
        }

        // console.log(scene)
    }, 500)

    // scene.overrideMaterial = new THREE.MeshBasicMaterial({ color: "magenta" });
}

_("btn-loader").addEventListener('click', () => {
    closeWelcome()


})


function closeWelcome() {
    dom.removeClass('btn-loader', 'show')
    dom.addClass('img-logo-loader-container', 'untransit')
    dom.addClass('loader', 'hide')

    // MODELS.moveContainerToRight(true, 2)
    // MODELS.rotateContainerToRight(true, 2)

    LABELS.animacionLabelsInicio()

}


function animate() {
    if (debugMode) stats.begin();
    CAM.update()

    renderer.render(scene, CAM.camera);
    LABELS.render(scene, CAM.camera)
    //   currentModel.rotation.y += 0.0015;

    if (debugMode) stats.end();
    requestAnimationFrame(animate);
}

