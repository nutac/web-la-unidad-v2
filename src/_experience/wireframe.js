import * as THREE from 'three';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import * as dom from "./dom-functions"

let scene, camera, renderer, controls;
let params

// init();

export function initWireframe(_fbx, container) {

    const fbx = _fbx.clone()

    fbx.traverse((child) => {
        if (child.isMesh) {
            if (child.material) {

                // child.material = child.material.clone(); // Clona el material para no modificar el original
                child.material.wireframe = true;
                child.material.transparent = true;
                child.material.opacity = 1; // Comienza como transparente
            }
        }
    });

    // console.log(fbx)
    container.add(fbx);
    fbx.visible = true



}
