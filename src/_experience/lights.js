import * as THREE from 'three';
import { params, gui, debugMode } from './params';

let ambientLight
let light1
let light2

// var params = {
//     ambientIntensity: 1.51,

//     light1_intensity: 1.9,
//     light1_x: 0.33,
//     light1_y: 1,
//     light1_z: 1.07,
//     light1_color: "#ffffff",

// };

// poner variables de luz aca


export function setup(scene) {
    // console.log(params)
    ambientLight = new THREE.AmbientLight(
        '#ffffff',
        1);
    scene.add(ambientLight);

    light1 = new THREE.DirectionalLight("#ffffff", 1);
    light1.castShadow = true
    light1.shadow.camera.near = 0.01;
    // light1.shadow.bias = 0.004;
    light1.shadow.bias = params.bias;


    let mapSizeScale = 6
    if (debugMode) mapSizeScale = 1
    light1.shadow.mapSize.width = 1024 * mapSizeScale; // Puedes ajustar esto más bajo o más alto
    light1.shadow.mapSize.height = 1024 * mapSizeScale;

    scene.add(light1);
    updateLight1()
    updateAmbientLightIntensity(params.ambientIntensity)
}
// export function addToContainer(container){

// }

export function updateAmbientLightIntensity(intensity) {
    ambientLight.intensity = intensity;
}

export function updateLight1() {
    light1.intensity = params.light1_intensity
    light1.position.set(params.light1_x, params.light1_y, params.light1_z);
    light1.color.set(params.light1_color);
}

export function setBias() {
    light1.shadow.bias = params.bias
}

