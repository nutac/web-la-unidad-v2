import * as THREE from 'three';
import { CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { CSS2DRenderer } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import * as DOM from '../dom-functions'
import { element } from 'three/examples/jsm/nodes/Nodes.js';
import { setRaycasterEnabled } from './raycaster';


let labelOffsets = {
    'ZonaGastronomico': { pos: { x: 0, y: -8, z: 10 } },
    'ZonaMuseo': { pos: { x: 0, y: 0, z: 10 } },
    'ZonaPoloTecnologico': { pos: { x: 0, y: 11, z: -13 } },
    'zonaShopping': { pos: { x: 0, y: 12, z: 13 } },
    'ZonaEstacionamiento': { pos: { x: 0, y: 5, z: 10 } },
}

let textos = {
    'zonaShopping': "Centro de Compras",
    'ZonaGastronomico': "Polo Gastronómico",
    'ZonaEstacionamiento': "Estacionamiento y Espacios Verdes",
    'ZonaMuseo': "Museo de la Cárcel y Centro de Ciencias",
    'ZonaPoloTecnologico': "Centro de Innovación",
}
export let divRenderer
let labels

export function setupRenderer2D() {
    divRenderer = new CSS2DRenderer();
    divRenderer.setSize(window.innerWidth, window.innerHeight)
    divRenderer.domElement.style.position = 'fixed'
    divRenderer.domElement.style.top = '0px'
    divRenderer.domElement.style.pointerEvents = 'none'
    divRenderer.domElement.style.width = '95% !important'
    document.body.appendChild(divRenderer.domElement)
}
export function render(scene, camera) {
    divRenderer.render(scene, camera);
}
// export function setSize(width, height){
//     divRenderer.setSize(width, height);
// }
export function setupLabels2D(zonasGroup, container) {
    let labelCSSClass = 'labelOnCanvas ';
    // if (!DOM.isMobile()) {
        labelCSSClass += 'mini'
    // }

    // Object.keys(modelGroups).forEach(modelName => {
    zonasGroup.children.forEach(child => {
        let modelName = child.name
        // console.log(modelName)

        // const group = modelGroups[modelName];
        const group = child;

        const box = new THREE.Box3().setFromObject(group);
        const center = box.getCenter(new THREE.Vector3());

        const labelDiv = document.createElement('div');
        labelDiv.className = labelCSSClass;
        labelDiv.id = 'label_' + modelName
        labelDiv.textContent = textos[modelName];

        if (labelOffsets[modelName] && labelOffsets[modelName].pos) {
            const offset = labelOffsets[modelName].pos;
            center.x += offset.x;
            center.y += offset.y;
            center.z += offset.z;
        }

        const div = document.createElement('div')
        div.appendChild(labelDiv)
        const divContainer = new CSS2DObject(div)
        divContainer.position.copy(center);

        container.add(divContainer)
        // renderer.domElement.appendChild(labelObj.element);

    });
    labels = DOM.getByClass('labelOnCanvas')
}

export function animacionLabelsInicio() {
    setRaycasterEnabled(false)
    setTimeout(function () {

        showLabelsAnimation(4)
        notminiLabelsAnimation(4)

        setTimeout(function () {
            // if (DOM.isMobile()) {
            //     hideLabelsAnimation(4)
            // } else {
                miniLabelsAnimation(4)
            // }
            setRaycasterEnabled(true)
        }, 2000)
    }, 2000)

}
export function anmacionLabelsScroll(mostrar) {
    // if (!DOM.isMobile()) {
    // setRaycasterEnabled(false)
    setTimeout(function () {
        if (mostrar) {
            showLabelsAnimation(4)
        notminiLabelsAnimation(4)
            
            setTimeout(function () {
                if (DOM.isMobile()) {
                    // hideLabelsAnimation(4)
                } else {
                }
                miniLabelsAnimation(4)

            }, 2000)
        } else {
            hideLabelsAnimation(4)
        }
    }, 1000)
    // }
}
function showLabelsAnimation(_index) {
    // console.log("SHOW")
    setTimeout(function () {
        if (_index < 0) {
            return
        } else {
            // console.log(_index, getLabelByIndex(_index))
            DOM.addClass(getLabelByIndex(_index), 'selected')
            // DOM.addClass(getLabelByIndex(_index), 'notmini')

            showLabelsAnimation(_index - 1)
        }
    }, 150)
}

function hideLabelsAnimation(_index) {
    setTimeout(function () {
        if (_index < 0) {
            return
        } else {
            DOM.removeClass(getLabelByIndex(_index), 'selected')
            // DOM.addClass(getLabelByIndex(_index), 'notmini')

            hideLabelsAnimation(_index - 1)
        }
    }, 100)
}
function miniLabelsAnimation(_index) {
    // console.log("SHOW")
    setTimeout(function () {
        if (_index < 0) {
            return
        } else {
            // console.log(_index, getLabelByIndex(_index))
            DOM.removeClass(getLabelByIndex(_index), 'notmini')

            miniLabelsAnimation(_index - 1)
        }
    }, 150)
}
function notminiLabelsAnimation(_index) {
    setTimeout(function () {
        if (_index < 0) {
            return
        } else {
            DOM.addClass(getLabelByIndex(_index), 'notmini')

            notminiLabelsAnimation(_index - 1)
        }
    }, 100)
}

function getLabelByIndex(index) {
    let modelName = ""
    if (index == 4) {
        modelName = "ZonaEstacionamiento"
    } else if (index == 3) {
        modelName = "zonaShopping"
    } else if (index == 2) {
        modelName = "ZonaPoloTecnologico"
    } else if (index == 1) {
        modelName = "ZonaMuseo"
    } else if (index == 0) {
        modelName = "ZonaGastronomico"

    }
    return DOM._('label_' + modelName)
}