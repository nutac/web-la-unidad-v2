import { debugMode } from './params';


/////////////// KEYBOARD OFFSETS ADJUSTMENTS ///////////////
export function moveObjectWithKeys(obj) {
    if (debugMode) {
        document.addEventListener('keydown', function (event) {
            let stepSize = 0.05;
            let rotationStep = 0.005;

            // let target = 'estacionamiento'
            // let obj = modelGroups[target]


            switch (event.key) {
                case 'ArrowUp':
                    obj.position.y += stepSize;
                    event.preventDefault();
                    break;
                case 'ArrowDown':
                    obj.position.y -= stepSize;
                    event.preventDefault();
                    break;
                case 'ArrowLeft':
                    obj.position.x -= stepSize;
                    break;
                case 'ArrowRight':
                    obj.position.x += stepSize;
                    break;
                case 'a':
                    obj.position.z -= stepSize;
                    break;
                case 'z':
                    obj.position.z += stepSize;
                    break;

                /// ROTAR
                case '8':
                    obj.rotation.x -= rotationStep;
                    break;
                case '2':
                    obj.rotation.x += rotationStep;
                    break;
                case '4':
                    obj.rotation.y -= rotationStep;
                    break;
                case '6':
                    obj.rotation.y += rotationStep;
                    break;

            }
            // console.log(`New camera position: x=${camera.position.x}, y=${camera.position.y}, z=${camera.position.z}`);
            let x = obj.position.x
            let y = obj.position.y
            let z = obj.position.z
            console.log("pos: { x:" + x + ", y: " + y + ", z: " + z + "},")

            x = obj.rotation.x
            y = obj.rotation.y
            z = obj.rotation.z
            console.log("rot: { x:" + x + ", y: " + y + ", z: " + z + "},")
        });
    }
}