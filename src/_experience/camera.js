import * as THREE from 'three';
// import { datosZona } from '../datosZonas';
import * as dom from "../dom-functions"
import { _ } from "../dom-functions"
import { debugMode } from './params';
// let debugMode=false
import { sectionNumber } from '../scroll';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
// import * as LABELS from './labels2d'
const USE_ORBIT_CONTROLS = false
let moveCameraWithKeys = false

let transitionActive = false;
let transitionStartTime = 0;
let transitionDuration = 2000; // Duración de la animación en milisegundos, ajusta según necesidad

let cameraStartPosition = new THREE.Vector3(0, 0, 0);
let cameraEndPosition = new THREE.Vector3(0, 0, 0);

let positionIndex = -1
const desktopCameraPositions = [
    new THREE.Vector3(-0.5862422064778933, 0.3099999999999993, 0.8092001717313209),
    new THREE.Vector3(-0.09000000000000005, -0.030000000000000016, 0.5999999999999969),
    new THREE.Vector3(-0.11999999999999995, 0.009999999999999969, 0.4199999999999995),
    new THREE.Vector3(0.4800000000000003, 0.030000000000000006, 0.05999999999999981),
    new THREE.Vector3(0.4500000000000006, -0.03, -0.5200000000000001),
    new THREE.Vector3(-0.37999999999999984, 0.08, -0.6500000000000002),
    new THREE.Vector3(0.06000000000000043, 0.03000000000000002, 0.7000000000000006),
    new THREE.Vector3(0.06000000000000043, 0.03000000000000002, 0.7000000000000006),
];
const mobileCameraPositions = [
    new THREE.Vector3(0.2337577935221073, 0.4699999999999994, 1.029200171731321),
    new THREE.Vector3(-0.08000000000000008, -0.05000000000000003, 0.739999999999997),
    new THREE.Vector3(0.02000000000000003, -1.734723475976807e-17, 0.5499999999999996),
    new THREE.Vector3(0.7400000000000005, 0.3200000000000002, 0.16999999999999982),
    new THREE.Vector3(0.48000000000000065, -0.08000000000000003, -0.5500000000000002),

    new THREE.Vector3(-0.6200000000000001, -0.029999999999999985, 0.17000000000000023),
    new THREE.Vector3(0.06000000000000043, 0.03000000000000002, 0.7000000000000006),
    new THREE.Vector3(0.06000000000000043, 0.03000000000000002, 0.7000000000000006),
];


const cameraPositions = dom.isMobile() ? mobileCameraPositions : desktopCameraPositions;


const cameraPositionsExtra = [
    // new THREE.Vector3(-0.5399999999999999, -0.029999999999999992, 0.010000000000000087),
    new THREE.Vector3(-0.4999999999999999, -0.029999999999999985, 0.07000000000000013),
]

export let camera;
let cameraContainer;
let controls;

export function setup(renderer, scene) {
    camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.01, 30);
    // scene.add(camera)
    camera.position.set(cameraPositions[0].x, cameraPositions[0].y, cameraPositions[0].z);
    // camera.position.set(0, 0, 0);

    camera.lookAt(0, 0, 0)

    // cameraContainer = new THREE.Group();
    // cameraContainer.add(camera)
    // cameraContainer.position.set(cameraPositions[0].x, cameraPositions[0].y, cameraPositions[0].z);
    // scene.add(cameraContainer)

    if (USE_ORBIT_CONTROLS) {
        controls = new OrbitControls(camera, renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.enableZoom = true;
    }

    if (moveCameraWithKeys) {
        setupMoveCameraWithKeys()
    }
    // createPoints(scene) 
    if (debugMode) {
    }
}

export function onWindowResize(renderer, divRenderer) {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    divRenderer.setSize(window.innerWidth, window.innerHeight);
}

// let lastSectionNumber = -1
// let counter = 0
// let count = true
// let lastIndex = 0
// let lastPointNear = -1
export function update() {
    if (USE_ORBIT_CONTROLS) controls.update();

    if (transitionActive) {
        let duration = transitionDuration
        // if (positionIndex == 5) {
        //     duration *= 0.5
        // }
        let elapsedTime = Date.now() - transitionStartTime;
        let t = elapsedTime / duration;
        t = easeInOutQuad(t);
        // console.log(t)
        if (t >= 0.99999) {
            t = 1;
            transitionActive = false; // Termina la animación
        }
        camera.position.lerpVectors(cameraStartPosition, cameraEndPosition, t);
        // cameraContainer.position.lerpVectors(cameraStartPosition, cameraEndPosition, t);

        camera.lookAt(0, 0, 0)
        // lastSectionNumber = sectionNumber

        if (t == 1) {
            if (positionIndex == 5) {
                if (!dom.isMobile()) {

                    startCameraAnimation(0, true)
                }

            }
        }
    }

    // if (path) {
    //     let total = cameraPositions.length + 1
    //     if (count) {
    //         counter++

    //         const threshold = 0.25;
    //         const cameraPos = camera.position;
    //         let isNearPoint = false;

    //         for (let i = 0; i < points.length; i++) {
    //             let point = points[i];

    //             if (i <= lastPointNear) continue

    //             // Verificación rápida usando un "bounding box"
    //             if (Math.abs(cameraPos.x - point.x) <= threshold &&
    //                 Math.abs(cameraPos.y - point.y) <= threshold &&
    //                 Math.abs(cameraPos.z - point.z) <= threshold) {
    //                 // Calculo de distancia más preciso sin raíz cuadrada
    //                 let dx = cameraPos.x - point.x;
    //                 let dy = cameraPos.y - point.y;
    //                 let dz = cameraPos.z - point.z;
    //                 let distSquared = dx * dx + dy * dy + dz * dz;
    //                 let precisionThresholdSquared = 0.05 * 0.05; // Suponiendo una precisión de 0.01 para el cálculo de distancia

    //                 if (distSquared <= precisionThresholdSquared) {
    //                     isNearPoint = true;
    //                     console.log("Cámara cerca del punto:", i, distSquared);
    //                     lastPointNear = i;
    //                     // count = false;
    //                     break;
    //                 }
    //             }
    //         }

    //         // const time = Date.now()
    //         // let index = time / 3000 % total
    //         let index = counter / 150 % total
    //         if (index == 0) lastPointNear = -1
    //         if (index > 1 && index < 4) {
    //             counter++
    //             counter++
    //             counter++

    //             index = counter / 150 % total

    //         }
    //         let indexRnd = Math.floor(index)
    //         // console.log(lastIndex, indexRnd)
    //         // console.log(index)
    //         if (indexRnd != lastIndex) {
    //             // count = false;
    //         }
    //         lastIndex = indexRnd

    //         const t = (index) / (total)
    //         // console.log(t, index)
    //         const pos = path.getPointAt(t)
    //         // pos.multiplyScalar(1.1)

    //         if (!debugDontMoveCamera) {
    //             camera.position.copy(pos)
    //             if (debugDontLookCenter) {
    //                 const tangent = path.getTangentAt(t).normalize()
    //                 camera.lookAt(pos.clone().add(tangent))
    //             } else {
    //                 camera.lookAt(0, 0, 0)
    //             }
    //         } else {
    //             redSphere.position.copy(pos)

    //         }
    //     }

    // }

}

export function startCameraAnimation(index, extra = false) {
    positionIndex = index;
    let targetPosition = cameraPositions[index]
    if (extra) {
        targetPosition = cameraPositionsExtra[index]
    }

    transitionActive = true;
    transitionStartTime = Date.now();

    cameraStartPosition.copy(camera.position);
    cameraEndPosition.copy(targetPosition);
}

function easeInOutQuad(t) {
    if (t === 1 || t > 1) {
        return 1;
    }
    return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
}

////////////////////////////////////////////////////////////
// if (debugMode && moveCameraWithKeys) {
function setupMoveCameraWithKeys() {
    document.addEventListener('keydown', function (event) {
        const stepSize = 0.01; // Define cuánto quieres mover la cámara con cada tecla
        const rotationStep = 0.01;
        let rotar = false
        let obj = camera
        if (!transitionActive) {
            switch (event.key) {
                case 'ArrowUp':
                    obj.position.y += stepSize;
                    event.preventDefault();
                    break;
                case 'ArrowDown':
                    obj.position.y -= stepSize;
                    event.preventDefault();
                    break;
                case 'ArrowLeft':
                    obj.position.x -= stepSize;
                    break;
                case 'ArrowRight':
                    obj.position.x += stepSize;
                    break;
                case 'a':
                    obj.position.z -= stepSize;
                    break;
                case 'z':
                    obj.position.z += stepSize;
                    break;
                case '8': // Arriba
                    obj.rotation.x -= rotationStep;
                    rotar = true
                    break;
                case '2': // Abajo
                    obj.rotation.x += rotationStep;
                    rotar = true
                    break;
                case '4': // Izquierda
                    obj.rotation.y -= rotationStep;
                    rotar = true
                    break;
                case '6': // Derecha
                    obj.rotation.y += rotationStep;
                    rotar = true
                    break;

            }
            // console.log(`New camera position: x=${camera.position.x}, y=${camera.position.y}, z=${camera.position.z}`);
            let x = obj.position.x
            let y = obj.position.y
            let z = obj.position.z
            // console.log(sectionNumber)
            // console.log("new THREE.Vector3(" + x + ", " + y + ", " + z + "),")
            console.log("new THREE.Vector3(" + x + ", " + y + ", " + z + "),")
            if (!rotar) {
                camera.lookAt(0, 0, 0)
            } else {

                console.log(obj.rotation.x, obj.rotation.y, obj.rotation.z)
            }

        }
    });
}

////////////////////////////////////////////////////////////


// let spheres = []
// let path
// let redSphere;

// let points = []
// let debugDontMoveCamera = false
// let debugDontLookCenter = false
// export function createPoints(container) {
//     const geometry = new THREE.SphereGeometry(0.005125, 16, 8)
//     cameraPositions.forEach((position, index) => {
//         const material = new THREE.MeshStandardMaterial({
//             color: 0xffffff * Math.random(),
//             side: THREE.DoubleSide,
//             transparent: true,
//             opacity: 0.75,
//         })
//         const sphere = new THREE.Mesh(geometry, material)

//         let mult = 0.93
//         let p = new THREE.Vector3(position.x * mult, position.y * mult, position.z * mult)
//         points.push(p)

//         sphere.position.set(p.x, p.y, p.z)
//         container.add(sphere)
//         spheres.push(sphere)
//     })
//     // console.log(spheres)
//     spheres.forEach((s) => {
//         console.log(s.position)
//     })
//     console.log(container)

//     /// red
//     const geometry2 = new THREE.SphereGeometry(0.025, 16, 8)

//     const material = new THREE.MeshStandardMaterial({
//         color: 0xff0000,
//         side: THREE.DoubleSide,
//         transparent: true,
//         opacity: 1,
//     })
//     redSphere = new THREE.Mesh(geometry2, material)
//     container.add(redSphere)


//     /// line
//     // "centripetal" | "chordal" | "catmullrom"
//     let curveType = 'catmullrom'
//     path = new THREE.CatmullRomCurve3(cameraPositions, true, curveType)
//     let totalSamples = 200
//     const samples = path.getPoints(totalSamples);
//     // const geo = new THREE.BufferGeometry().setFromPoints(path.getPoints(100))
//     const geo = new THREE.TubeGeometry(
//         new THREE.CatmullRomCurve3(samples, true, curveType),
//         totalSamples, 0.015, 8, false);
//     // const mat = new THREE.LineBasicMaterial({ color: 0xff0000 })
//     const mat = new THREE.MeshStandardMaterial({
//         color: 0x00ff00,
//         side: THREE.DoubleSide,
//         transparent: true,
//         opacity: 0.5,
//     });
//     const line = new THREE.Line(geo, mat)
//     // const line = new THREE.Mesh(geo, mat)
//     container.add(line)



// }
// document.addEventListener('keydown', function (event) {
//     switch (event.key) {
//         case 'c':
//             count = true
//             console.log(count)
//             break
//         case 'v':
//             debugDontMoveCamera = !debugDontMoveCamera
//             break
//         case 'b':
//             debugDontLookCenter = !debugDontLookCenter
//             break
//     }
// })