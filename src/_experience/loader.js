// import * as dom from "../dom-functions"
let loadingBar = document.getElementById('loadingBar')
let pct = 0
let modelPct = 0
let lastModelPCt = -1;
// let step = Math.round(100 / 12) + 1;
let step = (Math.ceil(100 / 6) + 1) * 0.3;

export function incrementLoaderPct() {
    pct += step;
    // let newValue = pct
    // console.log("PCT:", newValue)
    updateProgress()

}
export function updateModelPct(value) {
    if ((value * 0.7) > modelPct) {
        modelPct = value * 0.7
        // console.log(value) 
        updateProgress()
    }
}

function updateProgress() {
    // requestAnimationFrame(() => {
    let total = pct + modelPct
    // dom.setCssVariable('loader-pct', total + '%')
    // console.log("pct: ", pct, ", modelPct: ", modelPct, ", total: ", total)
    if (total > 100) total = 100
    // console.log("modelPct: ", modelPct, ", total: ", total)

    loadingBar.style.transform = `scaleX(${total}%)`
    // });
}
