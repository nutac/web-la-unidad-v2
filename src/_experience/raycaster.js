import * as THREE from 'three';
import * as CAM from './camera'
import * as DOM from '../dom-functions'
import { scrollToSection } from '../navBar';

/////////////// RAYCASTING ///////////////
const raycaster = new THREE.Raycaster();
const mouse = new THREE.Vector2();
let container, modelsToLoad
let enabled = true

let zonaParentNames = [
    'zonaShopping',
    'ZonaGastronomico',
    'ZonaEstacionamiento',
    'ZonaMuseo',
    'ZonaPoloTecnologico',

]
export function setup(renderer, _container, _modelsToLoad) {
    container = _container
    modelsToLoad = _modelsToLoad
    renderer.domElement.addEventListener('mousemove', onMouseMove);
    renderer.domElement.addEventListener('click', onClick);
}
export function setRaycasterEnabled(value) {
    enabled = value
    if (value == false) {
        hoveringZone('base')
    }
}

// function onMouseMove(event) {
//     if (!enabled) return


//     mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
//     mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

//     raycaster.setFromCamera(mouse, CAM.camera);

//     const intersects = raycaster.intersectObjects(container.children, true);

//     if (intersects.length > 0) {
//         const modelName = findModelName(intersects[0].object);
//         if (modelName) {
//             hoveringZone(modelName);
//         }
//         // console.log(intersects[0])
//     } else {
//         hoveringZone('base')
//     }
// }

function onMouseMove(event) {
    if (!enabled) return;

    updateMousePosition(event);
    processRaycasting(hoveringZone);
}

function onClick(event) {
    if (!enabled) return;

    updateMousePosition(event);
    processRaycasting(clickOnModel);
}

function updateMousePosition(event) {
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

function processRaycasting(callback) {
    raycaster.setFromCamera(mouse, CAM.camera);
    const intersects = raycaster.intersectObjects(container.children, true);

    if (intersects.length > 0) {
        const modelName = findModelName(intersects[0].object);
        if (modelName) {
            callback(modelName);
        }
    } else {
        callback('base');
    }
}


function hoveringZone(modelName) {
    // console.log(`Hovering over zone: ${modelName}`);
    // let elements = document.querySelectorAll('.labelOnCanvas.labels')
    // let labels = Array.from(elements)
    // labels.forEach(label => {
    // DOM.removeClass(label, 'selected')
    // })
    // console.log("optimizar hovering")

    // if (!DOM.isMobile()) {
    // let selectedLabel = document.querySelector('.labelOnCanvas.selected');
    // if (selectedLabel) {
    let notMiniLabel = document.querySelector('.labelOnCanvas.notmini');
    if (notMiniLabel) {
        // DOM.removeClass(selectedLabel, 'selected');
        if (!DOM.isMobile()) {
            DOM.removeClass(notMiniLabel, 'notmini');
        }
    }
    setCursorPointer(false)
    if (modelName != 'base') {
        setCursorPointer(true)
        // DOM.addClass('label_' + modelName, 'selected')
        if (!DOM.isMobile()) {
            DOM.addClass('label_' + modelName, 'notmini')
            // console.log('label_' + modelName)
        }

    }
    // }

}

function clickOnModel(modelName) {
    console.log(modelName)
    switch (modelName) {

        case 'ZonaGastronomico':
            scrollToSection(1)
            break;
        case 'ZonaMuseo':
            scrollToSection(2)
            break;
        case 'ZonaPoloTecnologico':
            scrollToSection(3)
            break;
        case 'zonaShopping':
            scrollToSection(4)
            break;
        case 'ZonaEstacionamiento':
            scrollToSection(5)
            break;

        default:
            break;
    }
}

function findModelName(object) {
    if (!object.parent) {
        return null;
    }

    const modelName = object.parent.name;
    // if (modelsToLoad.some(([name]) => name === modelName)) {
    if (zonaParentNames.includes(modelName)) {
        return modelName;
    }


    return findModelName(object.parent);
}

function setCursorPointer(condicion) {
    let element = DOM._("canvasContainer")
    if (condicion) {
        element.style.cursor = 'pointer';
    } else {
        element.style.cursor = 'default';
    }
}