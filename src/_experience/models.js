import * as THREE from 'three';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';
import * as LABELS from './labels2d'
import * as RAYCASTER from './raycaster'
import { moveObjectWithKeys } from './moveObject';
import { incrementLoaderPct, updateModelPct } from './loader.js';
// import { mergeBu } from 'three/examples/jsm/utils/BufferGeometryUtils.js';
import { mergeGeometries } from 'three/examples/jsm/utils/BufferGeometryUtils.js';
import { gsap } from 'gsap'

const loadingManager = new THREE.LoadingManager(
    // LOADED
    () => {
        console.log("loaded")
    },
    // PROGRESS
    (itemUrl, itemsLoaded, itemsTotal) => {
        let progress = itemsLoaded / itemsTotal
        // console.log(progress)
        updateModelPct(progress * 100)
    }
)

// let basePath = 'assets2/la-unidad-bake-test2_obj/';
// let basePath = 'assets2/la-unidad-bake-test-512-2048-800spp_obj/';
let basePath = 'assets2/';
let modelsToLoad = [
    // ['base', 'modelo-clonado-con-texturas'],
    // ['base', 'la-unidad-bake-test'],
    // ['base', 'la-unidad-bake-test2'],
    // ['base', 'la-unidad-bake-test-512-2048-800spp'],

    // ['base', 'merged-by-materials-v2-with-zones-v2'],
    // ['base', 'merged-by-materials'],
    // ['base', 'merged-by-materials'],
    // ['base', 'simlab-model-with-zones-v1'],


    // ['base', 'la-unidad-revision3'],
    // ['base', 'la-unidad-revision3-merged-with-zones2'],

    // ['base', 'merged-by-materials-with-zones-v4'],
    ['base', 'modelo-anterior-con-zonas-alineando-modelo-nuevo-muros-claros-4'],







    // ['base', '0_base'],
    // ['base', 'modelo-clonado-con-texturas2'],
    // ['base', '4_shopping'],
];

let modelGroups = {};
export let container;
export let zonasGroup
let scene

export async function loadModels(_scene, renderer) {
    scene = _scene
    console.time('Carga de Modelos');
    container = new THREE.Object3D();
    container.name = 'container'
    scene.add(container);
    let scale = 0.005;
    container.scale.set(scale, scale, scale);
    container.rotation.x = -Math.PI / 2; // Rota 90 grados en X si es necesario
    container.rotation.y = 0; // Rota 90 grados en X si es necesario
    container.rotation.z = 0;

    const loadingPromises = modelsToLoad.map(([modelName, fileName], index) => {
        return loadModel(modelName, fileName, index);
    });
    await Promise.all(loadingPromises);


    // LABELS.setupLabels2D(modelGroups, container);
    LABELS.setupLabels2D(zonasGroup, container);

    // console.log('2 - Labels set')
    incrementLoaderPct()

    const box = new THREE.Box3().setFromObject(container);
    const center = box.getCenter(new THREE.Vector3());
    container.position.x += (container.position.x * 1 - center.x);
    container.position.y += (container.position.y * 1 - center.y * 0.5);
    container.position.z += (container.position.z * 1 - center.z);

    // RAYCASTER.setup(renderer, container, modelsToLoad)
    RAYCASTER.setup(renderer, zonasGroup, modelsToLoad)

    // console.log('3 - raycaster set')
    incrementLoaderPct()

    // moveObjectWithKeys(modelGroups['gastronomico'])
    // moveObjectWithKeys(container)
    // moveContainerToRight(true, 2)
    // rotateContainerToRight(true, 2)
}

async function loadModel(modelName, fileName, index) {
    /// GLB DRACO FILES
    const loader = new GLTFLoader(loadingManager);
    const dracoLoader = new DRACOLoader(loadingManager);
    dracoLoader.setDecoderPath('https://www.gstatic.com/draco/versioned/decoders/1.5.7/'); // Setea la ruta al decodificador Draco
    loader.setDRACOLoader(dracoLoader);
    const modelPath = `${basePath}${fileName}.glb`;
    // const loader = new FBXLoader();
    // const modelPath = `${basePath}${fileName}.fbx`;
    return new Promise((resolve, reject) => {
        loader.load(
            modelPath,
            (gltf) => {
                handleModelLoaded(modelName, gltf.scene);
                // handleModelLoaded(modelName, gltf);

                resolve();
            },
            // xhr => updateProgress(xhr, index),
            null,
            reject
        );
    });

}
function handleModelLoaded(modelName, object) {
    // console.log(object.children)
    let parent = object.children[0]

    // zonasGroup = parent.children[1].children[0]
    // let modelo = parent.children[0].children[0]

    // zonasGroup = parent.children[1]
    // let modelo = parent.children[0]

    let modelo = null;
    for (let i = 0; i < parent.children.length; i++) {
        const child = parent.children[i];
        if (child.name.toLowerCase().includes("modelo")) {
            modelo = child;
        } else if (child.name.toLowerCase().includes("zona")) {
            zonasGroup = child
        }
    }


    // console.log('ZONAS', zonasGroup)
    // console.log('MODELO', modelo)

    setZonesMaterial(zonasGroup)

    setupShadows(modelo)

    container.add(zonasGroup)
    container.add(modelo)

    // const group = new THREE.Group();
    // group.add(object);
    // group.name = modelName;
    // container.add(group);
    // modelGroups[modelName] = group;
}
function setZonesMaterial(zones) {
    // Creamos un material transparente verde
    const greenMaterial = new THREE.MeshStandardMaterial({
        color: '#00ff00',   // Color verde
        transparent: true,  // Habilitar transparencia
        opacity: 0.5,        // Nivel de transparencia
        visible: false
    });

    // Recorremos todos los descendientes de zones
    zones.traverse((child) => {
        // console.log(child)
        // Verificamos si el objeto es una malla y tiene un material
        if (child.isMesh && child.material) {
            child.material = greenMaterial;
        }
    });
}
function setupShadows(object) {
    object.traverse(function (child) {
        if (child.isMesh) {

            // let name = child.name
            // if (name) {
            // if (name.includes('mesh')) {
            //     console.log('mesh generico:', name)
            // } else {
            //     console.log('mesh POSTA:', name)
            // }
            // console.log(child)
            child.castShadow = true;
            child.receiveShadow = true;
            // }
        }
    });

    // object.children.forEach(child => {
    //     child.castShadow = true;
    //     child.receiveShadow = true;
    //     console.log(child)
    // })
}

function mergeSameMaterialMeshes(object, container) {
    const materialsMap = {};
    let totalMeshes = 0;
    // Agrupar geometrías por material y almacenar una referencia al material
    object.traverse((child) => {
        if (child.isMesh) {
            const materialKey = child.material.uuid;
            if (!materialsMap[materialKey]) {
                materialsMap[materialKey] = {
                    geometries: [],
                    material: child.material // Almacenar la referencia del material original
                };
            }
            materialsMap[materialKey].geometries.push(child.geometry);
            totalMeshes++;
        }
    });
    console.log('Total meshes ORIGINAL: ', totalMeshes);

    // Fusionar geometrías que comparten el mismo material
    const mergedMeshes = [];
    for (const materialKey in materialsMap) {
        const geometries = materialsMap[materialKey].geometries;
        const mergedGeometry = mergeGeometries(geometries);
        const material = materialsMap[materialKey].material; // Usar el material original
        const mergedMesh = new THREE.Mesh(mergedGeometry, material);
        mergedMeshes.push(mergedMesh);
    }

    // Añadir mallas fusionadas a la escena
    totalMeshes = 0;
    mergedMeshes.forEach(mesh => {
        container.add(mesh);
        totalMeshes++;
    });
    console.log('Total meshes: ', totalMeshes);
}

export function moveContainerToRight(moveRight, duration = 3.0) {
    // return
    let from = -0.0248
    // let to = 0.0751
    let to = -0.3032748

    let from2 = 0
    // let to = 0.0751
    let to2 = 0.03

    let value = from
    let value2 = from2

    if (moveRight) {
        value = to
        value2 = to2
    }
    gsap.to(
        scene.position,
        {
            duration: duration,
            ease: 'power2.inOut',
            x: value,
            y: value2
        }
    )

    // let offset = -0.05
    // if (moveRight) {
    //     offset *= -1
    // }
    // container.position.x += offset
    // console.log(container.position.x)

}
export function rotateContainerToRight(moveRight, duration = 1.0) {
    // return
    let from = 0
    // let to = -0.1
    let to = -0.05

    let from2 = -1.57
    let to2 = -1.97

    let value = from
    let value2 = from2
    if (moveRight) {
        value = to
        value2 = to2
    }
    gsap.to(
        scene.rotation,
        {
            duration: duration,
            ease: 'power2.inOut',
            z: value,
            // x: value2
        }
    )

    // let offset = -0.05
    // if (moveRight) {
    //     offset *= -1
    // }
    // container.rotation.z += offset
    // console.log(container.rotation.z)
}
export function moveContainerDown(moveRight, duration = 1.0) {
    let from = 0
    let to = -0.05

    let value = from
    if (moveRight) {
        value = to
    }
    gsap.to(
        scene.position,
        {
            duration: duration,
            ease: 'power2.inOut',
            y: value,
        }
    )
}
document.addEventListener('keydown', function (event) {
    if (event.key == 'm') {
        // moveContainerToRight(true)
        scene.rotation.x -= 0.05
        console.log(scene.rotation.x)

    }
    if (event.key == 'n') {
        // moveContainerToRight(false)
        scene.rotation.x += 0.05
        console.log(scene.rotation.x)
    }
    if (event.key == 'k') {
        // rotateContainerToRight(true)
        scene.rotation.z -= 0.05
        console.log(scene.rotation.z)


    }
    if (event.key == 'j') {
        // rotateContainerToRight(false)
        scene.rotation.z += 0.05
        console.log(scene.rotation.z)


    }

})