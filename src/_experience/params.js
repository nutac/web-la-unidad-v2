// Importar dat.GUI
import * as dat from 'dat.gui';
import * as LIGHTS from './lights.js';
// import { debugMode } from './_main3d';

export let debugMode = false


export var gui
export var params = {
    ambientIntensity: 0.5,

    light1_intensity: 1.4,
    light1_x: 0.8,
    light1_y: 10,
    light1_z: 6,
    light1_color: "#ffffff",

    bias: -0.000012,

};

export async function setupParamsGui() {
    // console.log(params)
    // params = JSON.parse(JSON.stringify(defaultParams))
    gui = new dat.GUI();

    /// LIGHTS
    gui.add(params, 'ambientIntensity', 0, 5).onChange(value => {
        LIGHTS.updateAmbientLightIntensity(value);
    });

    gui.add(params, 'light1_intensity', 0, 2).onChange(LIGHTS.updateLight1);
    gui.add(params, 'light1_x', -10, 10).onChange(LIGHTS.updateLight1);
    gui.add(params, 'light1_y', -10, 10).onChange(LIGHTS.updateLight1);
    gui.add(params, 'light1_z', -10, 10).onChange(LIGHTS.updateLight1);
    gui.addColor(params, 'light1_color').onChange(LIGHTS.updateLight1);

    gui.add(params, 'bias', -0.00003, 0, 0.000001).onChange(LIGHTS.setBias)




    gui.closed = true

    if (!debugMode) {
        gui.hide()
    }

}