import * as dom from "../dom-functions"
import { _ } from "../dom-functions"

export function setup() {
    // _("btn-submit").onclick = () => sendContactFormMail();
}

function sendContactFormMail() {
    let data = getFormData();
    if (data.error) {
        // console.log("ERROR:", data.error)
        alert(data.error)
    } else {
        dom.removeClass("loader", "hide")
        // console.log("DATOS:", data)
        const url = 'https://us-central1-xrs-web-functions.cloudfunctions.net/sendEmailFromContactForm'
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            // .then(response => response.json())
            // .then(data => console.log(data))
            .then(response => {
                console.log(response)
                _("input_name").value = ''
                _("input_email").value = ''
                _("textarea_message").value = ''
                document.getElementById('modal-message-sent').classList.add('show')
                dom.addClass("loader", "hide")

            })
            .catch((error) => {
                console.error('Error:', error);
                dom.addClass("loader", "hide")

            });
    }
}
function getFormData() {
    let name = _("input_name").value
    let email = _("input_email").value
    let message = _("textarea_message").value

    if (name == '' || email == "" || message == "") {
        return {
            error: "Error: Debe completar todos los campos"
        }
    }

    return {
        name: name,
        email: email,
        message: message
    }
}