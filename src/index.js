import '@fortawesome/fontawesome-free/css/all.min.css'

import * as dom from "./dom-functions"
import { _ } from "./dom-functions"
import * as scroll from "./scroll.js"


// import { datosZona } from "./datosZonas.js";

import navBar_html from "./navBar.html?raw"
import { scrollToSection } from './navBar.js';


// import zona_html from "./sections/zona_data.html?raw"

import como_comenzo_html from "./sections/como-comenzo.html?raw"
import importancia_cultural_html from "./sections/importancia-cultural.html?raw"


import * as navbar from "./navBar.js"

// window.onload = function () {
export function setupSections() {
  _("navBar").innerHTML = navBar_html

  // _('sGastronomico').innerHTML = zona_html
  // _('sMuseo').innerHTML = zona_html
  // _('sInnovacion').innerHTML = zona_html
  // _('sShopping').innerHTML = zona_html
  // _('sEstacionamiento').innerHTML = zona_html

  // let dataZonas = document.querySelectorAll(".zonaData")
  // for (let i = 0; i < dataZonas.length; i++) {
  //   let titulo = dataZonas[i].querySelector('.zonaTitulo')
  //   titulo.innerHTML = datosZona[i].titulo
  //   let texto = dataZonas[i].querySelector('.zonaTexto')
  //   texto.innerHTML = datosZona[i].texto
  // }

  _("sectionComoComenzo").innerHTML = como_comenzo_html

  _("sectionImportanciaCultural").innerHTML = importancia_cultural_html

  navbar.setup("navBar")

  window.addEventListener('scroll', scroll.handleScroll);

  // if (!debugMode) {
  // scrollToSection(0)
  // }

  if (dom.isMobile()) {
    setupItemsComoInicioMobile()
    setImagenesMobile();

  } else {
    setupItemsComoInicio()
  }
  setInterval(() => changeComoInicioImage(), 3000)

  asignarEventosClickEnTitulos();
  // console.log("NOTAS: /////////////////////////////  \n -SONIDO! click en seccion y movimiento de camara")

}

/////////////////////////////////
let itemsComoInicio
let acordeons
let lastSelected = -1
let imageIndex = 0;
function setupItemsComoInicio() {
  itemsComoInicio = dom.getByClass('itemListaComo')
  acordeons = dom.getByClass('acordeon')
  for (let i = 0; i < itemsComoInicio.length; i++) {
    itemsComoInicio[i].onclick = () => itemComoClick(i)
  }
}

function changeComoInicioImage() {
  imageIndex++
  if (imageIndex > 4) imageIndex = 0
  mostrarImagen(imageIndex)
}
function itemComoClick(index) {
  // return




  for (let i = 0; i < itemsComoInicio.length; i++) {
    dom.removeClass(itemsComoInicio[i], 'selected')
    dom.removeClass(acordeons[i], 'selected')

  }
  if (index != lastSelected) {
    dom.addClass(itemsComoInicio[index], 'selected')
    dom.addClass(acordeons[index], 'selected')
    lastSelected = index
  }

  // mostrarImagen(index)

  // if (index == 0) {
  //   document.getElementById("imgComoComenzo").src = "assets2/005b.png"
  // } else if (index == 1) {
  //   document.getElementById("imgComoComenzo").src = "assets2/001b.png"

  // } else if (index == 2) {
  //   document.getElementById("imgComoComenzo").src = "assets2/003.png"

  // }
}
function mostrarImagen(index) {

  const imgContainer = document.getElementById("imgContainerComoComenzo")
  const imagenes = imgContainer.querySelectorAll('img');
  // console.log(imagenes[index].src)
  imagenes.forEach((img, i) => {
    img.classList.toggle('selected', i === index);
  });

  // console.log(index)

}
function setupItemsComoInicioMobile() {
  document.addEventListener("DOMContentLoaded", function () {
    const slider = document.querySelector('.slider-wrapper');
    const dotsWrapper = document.querySelector('.dots-wrapper');

    // Crear un punto por cada elemento en el carrusel
    for (let i = 0; i < slider.children.length; i++) {
      const dot = document.createElement('span');
      dot.classList.add('dot');
      dot.addEventListener('click', () => {
        slider.scrollLeft = i * slider.offsetWidth;
        updateActiveDot(i);

      });
      dotsWrapper.appendChild(dot);
    }

    // Función para actualizar el punto activo
    function updateActiveDot(index) {
      const dots = dotsWrapper.querySelectorAll('.dot');
      dots.forEach((dot, i) => {
        dot.classList.toggle('active', i === index);
      });
      // mostrarImagen(index)

    }

    // Actualizar el punto activo cuando se desplace el carrusel
    slider.addEventListener('scroll', () => {
      const index = Math.round(slider.scrollLeft / slider.offsetWidth);
      updateActiveDot(index);
    });
  });

}

function setImagenesMobile() {
  /// COMO COMENZO:
  const imgContainer = document.getElementById('imgContainerComoComenzo');
  const imagenesMobile = [
    'assets2/como_inicio_mobile1.png',
    'assets2/como_inicio_mobile2.png',
    'assets2/como_inicio_mobile3.png',
    'assets2/como_inicio_mobile4.png',
    'assets2/como_inicio_mobile5.png',
  ];

  const imagenes = imgContainer.querySelectorAll('img');
  imagenes.forEach((imagen, index) => {
    imagen.src = imagenesMobile[index];
  });

  /// IMPORTANCIA CULTURAL
  document.getElementById('imgImportancia').src = 'assets2/valdes_mobile.png'
  // document.getElementById('imgImportanciaMobile').src = 'assets2/foto historica 3.png'


}


function asignarEventosClickEnTitulos() {
  const selectores = [
    '#sGastronomico .title',
    '#sMuseo .title',
    '#sInnovacion .title',
    '#sShopping .title',
    '#sEstacionamiento .title'
  ];

  selectores.forEach(selector => {
    const elemento = document.querySelector(selector);
    if (elemento) {
      elemento.addEventListener('click', () => navbar.scrollToHome());
    } else {
      console.warn(`Elemento no encontrado: ${selector}`);
    }
  });
}