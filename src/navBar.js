import * as dom from "./dom-functions.js"
import { _ } from "./dom-functions"

import smoothscroll from 'smoothscroll-polyfill';
smoothscroll.polyfill();

import { startCameraAnimation } from "./_experience/camera.js";
import { moveContainerToRight, rotateContainerToRight } from "./_experience/models.js";
import { setRaycasterEnabled } from "./_experience/raycaster.js";

import { log } from './log.js'
import * as labels2d from './_experience/labels2d.js'

let menu_items = [];
let menu_subitems = [];
let product_list_item = []
export let sections = []

const menu_products = 2;
const SECTION_HOME = 0;
const SECTION_PRODUCTS_LIST = 2;
const SECTION_XRS_WEB = 3;
const SECTION_XRS_MOBILE = 4;
const SECTION_XRS_VR = 5;

const S_HOME = 0
const S_GASTRONOMICO = 1;
const S_MUSEO = 2
const S_INNOVACION = 3
const S_SHOPPING = 4
const S_ESTACIONAMIENTO = 5
const S_COMO_COMENZO = 6
const S_IMPORTANCIA = 7



let side_menu_items = []
let menu_burger_is_open = false;
// let allSectionsHidden = false
let showing_product = false;
let clickAudio

export function setup() {
    sections = document.getElementsByClassName("section")

    menu_items = document.getElementsByClassName("menu-item")
    for (let i = 0; i < menu_items.length; i++) {
        menu_items[i].onclick = () => menuItemClick(i);
    }

    side_menu_items = document.getElementsByClassName("side-menu-item")
    for (let i = 0; i < side_menu_items.length; i++) {
        side_menu_items[i].onclick = () => sideMenuItemClick(i);
    }

    _("ham").onclick = () => {
        if (menu_burger_is_open) {
            showSideMenu(false)
        } else {
            showSideMenu(true)
        }
    }

    clickAudio = new Audio('/assets2/click.mp3');

    window.addEventListener('sectionchange', function (event) {
        let currentSection = event.detail.section
        currentSection--
        // console.log(currentSection)

        // if (currentSection <= S_ESTACIONAMIENTO) {
        // }
        // else 
        // if (currentSection > S_ESTACIONAMIENTO) {
        // selecMenuItem(currentSection - S_ESTACIONAMIENTO)

        // dom.addClass("dummyComoComenzo", "hide")

        startCameraAnimation(currentSection)
        if (currentSection == S_HOME) {
            dom.addClass('canvasContainer', 'move')
            setRaycasterEnabled(true)
            // console.log("mostrar labels")
            labels2d.anmacionLabelsScroll(true)
        } else {
            dom.removeClass('canvasContainer', 'move')
            setRaycasterEnabled(false)
            // console.log("OCULTAR labels")
            labels2d.anmacionLabelsScroll(false)
        }

        if (currentSection <= S_COMO_COMENZO) {
            selecMenuItem(S_HOME)
            dom.removeClass("canvasContainer", "hide")

            if (currentSection == S_COMO_COMENZO) {
                selecMenuItem(1)
                dom.addClass("canvasContainer", "hide")
                // dom.removeClass("dummyComoComenzo", "hide")
                // } else {
                // dom.addClass("dummyComoComenzo", "hide")
            }

        }
        else {
            dom.addClass("canvasContainer", "hide")
            selecMenuItem(currentSection - S_ESTACIONAMIENTO)

        }

        if (currentSection >= S_COMO_COMENZO) {
            dom.addClass("navBar", "dark")
        } else {
            dom.removeClass("navBar", "dark")
        }

        // if (currentSection == S_IMPORTANCIA) {
        //     dom.removeClass("dummyImportancia1", "hide")
        //     dom.removeClass("dummyImportancia2", "hide")
        //     dom.removeClass("dummyImportancia3", "hide")

        // } else {
        //     dom.addClass("dummyImportancia1", "hide")
        //     dom.addClass("dummyImportancia2", "hide")
        //     dom.addClass("dummyImportancia3", "hide")
        // }

        /// animacion lista como comenzo
        let listaComo = dom.getByClass('itemListaComo')
        if (currentSection == S_COMO_COMENZO ||
            currentSection == S_IMPORTANCIA
        ) {
            for (let i = 0; i < listaComo.length; i++) {
                dom.addClass(listaComo[i], 'show')
            }
            // dom.addClass('textoLaURepresenta', 'show')
            dom.addClass('imgContainerComoComenzo', 'show')

        } else {
            for (let i = 0; i < listaComo.length; i++) {
                dom.removeClass(listaComo[i], 'show')
            }
            // dom.removeClass('textoLaURepresenta', 'show')
            dom.removeClass('imgContainerComoComenzo', 'show')

        }

        if (currentSection == S_IMPORTANCIA) {
            dom.addClass('textoImportanciaCultural', 'show')
            dom.addClass('imgFirmaValdes', 'show')
            dom.addClass('imgImportancia', 'show')
            // dom.addClass('citaValdes', 'show')
        } else {
            dom.removeClass('textoImportanciaCultural', 'show')
            dom.removeClass('imgFirmaValdes', 'show')
            dom.removeClass('imgImportancia', 'show')
            // dom.removeClass('citaValdes', 'show')
        }

        // if (currentSection > S_COMO_COMENZO) {
        //     selecMenuItem(currentSection - S_COMO_COMENZO)

        // } else {
        //     selecMenuItem(S_HOME)
        //     startCameraAnimation(currentSection)
        // }

        if (dom.isMobile()) {

        } else {

            if (currentSection == S_HOME) {
                moveContainerToRight(true)
                rotateContainerToRight(true)
            } else {
                moveContainerToRight(false)
                rotateContainerToRight(false)
            }
        }

    });

    _("menu-burger").onclick = () => dom.toggleClass("menu-burger", "open")
    _("menu-logo").onclick = (e) => scrollToHome()
}


export function menuItemClick(index) {
    let sectionIndex = index;
    // if (index > menu_products) {
    //     sectionIndex += 4
    // }

    // selecMenuItem(index)
    let offsetSection = 0
    if (sectionIndex > 0) offsetSection = 5

    scrollToSection(sectionIndex + offsetSection)
};
function selecMenuItem(index) {
    for (let i = 0; i < menu_items.length; i++) {
        dom.removeClass(menu_items[i], 'selected')
    }
    dom.addClass(menu_items[index], 'selected')
}
export function scrollToSection(sectionIndex, event) {
    // if (event) event.stopPropagation()
    if (event) {
        event.preventDefault();
        event.stopPropagation();
    }
    clickSound()
    log("scroll-to-section: " + sectionIndex)
    document.documentElement.style.scrollSnapType = 'none';
    document.body.style.scrollSnapType = 'none';

    const sectionTop = sections[sectionIndex].offsetTop;
    window.scrollTo({
        top: sectionTop,
        behavior: 'smooth'
    });

    setTimeout(() => {
        document.documentElement.style.scrollSnapType = 'y mandatory';
        document.body.style.scrollSnapType = 'y mandatory';

    }, 1000)

    // sections[sectionIndex].scrollIntoView({
    //     // behavior: 'smooth',
    //     block: 'start'
    // });

}
function showSideMenu(show) {
    if (show) {
        dom.addClass("ham", "active")
        dom.addClass("mobile-side-menu", "show")
        menu_burger_is_open = true;

    } else {
        dom.removeClass("ham", "active")
        dom.removeClass("mobile-side-menu", "show")
        menu_burger_is_open = false;
    }
}

function sideMenuItemClick(index) {

    // scrollToSection(index)

    let offsetSection = 0
    if (index > 0) offsetSection = 5
    scrollToSection(index + offsetSection)

    // setTimeout( () => {
    showSideMenu(false)
    // }, 1000)
}
function showProduct(index) {
    showing_product = false
    for (let i = SECTION_XRS_WEB; i <= SECTION_XRS_VR; i++) {
        if (i == index) {
            dom.addClass(sections[i], "show")
            showing_product = true
        } else {
            dom.removeClass(sections[i], "show")
        }
    }
    updateShowingBtnBack()
}
function updateShowingBtnBack() {
    if (showing_product) {
        dom.removeClass("img-menu", "show")
        dom.addClass("btn-back", "show")
        // dom.addClass("navBar", "showBg")
    } else {
        dom.addClass("img-menu", "show")
        dom.removeClass("btn-back", "show")
        // dom.removeClass("navBar", "showBg")
    }

}
// function showAllSections(show) {
//     for (let i = 0; i < sections.length; i++) {
//         if (show) {
//             dom.removeClass(sections[i], "hide")
//         } else {
//             dom.addClass(sections[i], "hide")
//         }
//     }
// }

export function scrollToHome() {
    // showAllSections(true)
    // if (showing_product) {
    //     showProduct(-1)
    //     scrollToSection(SECTION_PRODUCTS_LIST)
    // } else {
    scrollToSection(SECTION_HOME)
    // }
    updateShowingBtnBack()
}


function clickSound() {
    clickAudio.currentTime = 0; // Reinicia el audio al principio
    clickAudio.play().catch(error => {
        console.warn('Error al reproducir el sonido:', error);
    });
}