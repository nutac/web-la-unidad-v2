export var _ = function (s) { return domget(s); }
export function domget(_control) {
    let element;
    if (typeof _control === "string" || _control instanceof String) {
        element = document.getElementById(_control);
    } else {
        element = _control;
    }
    return element;
}

export function addClass(element, class_name) {
    domget(element).classList.add(class_name)
}

export function removeClass(element, class_name) {
    domget(element).classList.remove(class_name)
}

export function getByClass(class_name) {
    return document.getElementsByClassName(class_name)
}
export function toggleClass(element, class_name) {
    domget(element).classList.toggle(class_name)
}

export function getCssVariable(name) {
    const style = getComputedStyle(document.documentElement);
    return style.getPropertyValue(`--${name}`).trim(); 
}
export function setCssVariable(name, value) {
    document.documentElement.style.setProperty(`--${name}`, value);
}

export function isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || (window.innerWidth <= 800);
}